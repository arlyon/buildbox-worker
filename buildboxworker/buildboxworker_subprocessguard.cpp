/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_subprocessguard.h>
#include <buildboxworker_worker.h>

namespace buildboxworker {

SubprocessGuard::SubprocessGuard(const pid_t &subprocessPid, Worker *worker)
    : d_worker(worker), d_subprocessPid(subprocessPid)
{
    d_worker->trackSubprocess(d_subprocessPid);
}

SubprocessGuard::~SubprocessGuard()
{
    d_worker->untrackSubprocess(d_subprocessPid);
}

} // namespace buildboxworker
