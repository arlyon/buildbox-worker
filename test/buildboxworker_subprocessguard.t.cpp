#include <gtest/gtest.h>

#include <buildboxworker_subprocessguard.h>
#include <buildboxworker_worker.h>

using namespace testing;
using namespace buildboxworker;

TEST(SubprocessGuardTests, TestSubprocessGuard)
{
    Worker worker;
    {
        EXPECT_EQ(0, worker.d_subprocessPgid.count(42));
        SubprocessGuard g(pid_t(42), &worker);
        EXPECT_EQ(1, worker.d_subprocessPgid.count(42));
    }
    EXPECT_EQ(0, worker.d_subprocessPgid.count(42));
}
