What is ``buildbox-worker``?
============================

``buildbox-worker`` is a program that accepts jobs from a build server, invokes
a ``buildbox-run`` command to run them, and sends the results back. It
implements Bazel's `Remote Workers API`_.

It is designed to work with `BuildGrid`_ but does not depend on it.

.. _Remote Workers API: https://docs.google.com/document/d/1s_AzRRD2mdyktKUj2HWBn99rMg_3tcPvdjx3MPbFidU
.. _BuildGrid: https://buildgrid.build


::

      Usage: ./buildbox-worker
         --help                         Display usage and exit [optional]
         --instance                     Instance name to pass to the Remote Workers API and the CAS servers [optional, default = ""]
         --concurrent-jobs              Number of jobs to run at once, defaults to running one job at a time [optional, default = 1]
         --stop-after                   Terminate after running the given number of jobs, defaults to non-terminating [optional, default = -1]
         --bots-remote                  URL for the Bots service [required]
         --bots-instance                Name of the Bots instance [optional, default = ""]
         --bots-server-cert             Public server certificate for Bots TLS (PEM-encoded) [optional]
         --bots-client-key              Private client key for Bots TLS (PEM-encoded) [optional]
         --bots-client-cert             Private client certificate for Bots TLS (PEM-encoded) [optional]
         --bots-access-token            Access Token for authentication Bots (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token [optional]
         --bots-googleapi-auth          Use GoogleAPIAuth for Bots service [optional, default = false]
         --bots-retry-limit             Number of times to retry on grpc errors for Bots service [optional, default = "4"]
         --bots-retry-delay             How long to wait in milliseconds before the first grpc retry for Bots service [optional, default = "1000"]
         --cas-remote                   URL for the CAS service [required]
         --cas-instance                 Name of the CAS instance [optional, default = ""]
         --cas-server-cert              Public server certificate for CAS TLS (PEM-encoded) [optional]
         --cas-client-key               Private client key for CAS TLS (PEM-encoded) [optional]
         --cas-client-cert              Private client certificate for CAS TLS (PEM-encoded) [optional]
         --cas-access-token             Access Token for authentication CAS (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token [optional]
         --cas-googleapi-auth           Use GoogleAPIAuth for CAS service [optional, default = false]
         --cas-retry-limit              Number of times to retry on grpc errors for CAS service [optional, default = "4"]
         --cas-retry-delay              How long to wait in milliseconds before the first grpc retry for CAS service [optional, default = "1000"]
         --logstream-remote             URL for the LogStream service [optional]
         --logstream-instance           Name of the LogStream instance [optional, default = ""]
         --logstream-server-cert        Public server certificate for LogStream TLS (PEM-encoded) [optional]
         --logstream-client-key         Private client key for LogStream TLS (PEM-encoded) [optional]
         --logstream-client-cert        Private client certificate for LogStream TLS (PEM-encoded) [optional]
         --logstream-access-token       Access Token for authentication LogStream (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token [optional]
         --logstream-googleapi-auth     Use GoogleAPIAuth for LogStream service [optional, default = false]
         --logstream-retry-limit        Number of times to retry on grpc errors for LogStream service [optional, default = "4"]
         --logstream-retry-delay        How long to wait in milliseconds before the first grpc retry for LogStream service [optional, default = "1000"]
         --request-timeout              Sets the timeout for updateBotSession request, defaults to 1 second [optional, default = 1]
         --buildbox-run                 Absolute path to runner exectuable [required]
         --runner-arg                   Arguments to pass buildbox-run when the worker runs a job
                                           This can be useful if the buildbox-run implementation you're using supports non-standard options [optional]
         --platform                     Add a key-value pair to the 'Platform' message the worker sends to the server
                                           (see the Remote Workers API specification for keys and values you can use)
                                           format is --platform KEY=VALUE [optional]
         --metrics-mode                 format is --metrics-mode=MODE where 'MODE' is one of:
                                           udp://<hostname>:<port>
                                           file:///path/to/file
                                           stderr [optional]
         --metrics-publish-interval     Metrics publishing interval [optional]
         --log-level                    Log verbosity level [optional, default = "info"]
         --verbose                      Set log level to debug [optional]
         --log-file                     Log file name [optional, default = ""]
         --config-file                  Absolute path to config file [optional, default = ""]
           BOT Id                       POSITIONAL [optional]

The ``buildbox-run`` interface
==============================

``buildbox-worker`` invokes a ``buildbox-run`` command to download a job's
files, run the job, and upload the results. You can change the command with
the ``--buildbox-run=BUILDBOXRUN`` option to change the fetching and
sandboxing mechanisms the worker uses.

The command you specify must support the following interface:

  BUILDBOXRUN --remote=URL [--instance=NAME] [--server-cert=CERT] [--server-key=KEY]
  [--client-key=KEY] --action=FILE --action-result=FILE

``--remote`` and  ``--instance`` (empty by default) specify the CAS server.
``--server-cert``, ``--server-key``, and ``--client-key`` work as they do in
``buildbox-worker``.  ``--action`` specifies the path to a file with a
serialized Action protocol buffer, and ``--action-result`` is the path to write
the corresponding ActionResult to.


The ``buildbox-worker configuration`` file
==============================

``buildbox-worker`` supports a protobuf-based configuration enabled via the
``--config-file=FILE`` parameter, where 'FILE' points to the absolute path of
the file. This file currently only supports a 'botStatus' entry. Format of the
file is:

botStatus: [0-4]

To be backwards compatible, if no ``--config-file=FILE`` is on the command-line,
the worker will set it's initial status to ``OK``.

Otherwise, the worker will read in the status from the config file and use that
as the initial state to be passed to a bots service.

Metrics
=======
buildbox-worker publishes runtime metrics throughout its lifetime. Documentation listing the names of these published metrics can be found in `buildboxworker_metricnames.cpp`_ and a brief description of them can be found in the `buildboxworker_metricnames.h`_ header file.

.. _buildboxworker_metricnames.cpp: https://gitlab.com/BuildGrid/buildbox/buildbox-worker/-/blob/master/buildboxworker/buildboxworker_metricnames.cpp
.. _buildboxworker_metricnames.h: https://gitlab.com/BuildGrid/buildbox/buildbox-worker/-/blob/master/buildboxworker/buildboxworker_metricnames.h

Metrics Enablement
------------------
To enable metrics publishing, two command line options are required:
::

   --metrics-mode=MODE                Options for MODE are:
           udp://<hostname>:<port>
           file:///path/to/file
           stderr

   --metrics-publish-interval=VALUE   Publish metric at the specified interval rate in seconds, defaults 15 seconds

Example #1: Write metrics to a statsd server on the local host listening on port 50051 and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=udp://localhost:50051 --metrics-publish-interval=5

Example #2: Write metrics to stderr and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=stderr --metrics-publish-interval=5

Example #3: Write metrics to a file and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=file:///tmp/my-metrics.log --metrics-publish-interval=5

Stdout/stderr streaming (LogStream API)
=======================================

buildbox-worker supports using the `LogStream API`_ to stream the contents that a command writes to its standard outputs in real time.

To request that, a remote execution server can attach an ``ExecuteOperationMetadata`` message with key ``"executeoperationmetadata-bin"`` to the `trailing metadata`_ returned by the ``UpdateBotSession()`` method.

That metadata message must contain the digest of the action to be streamed and a LogStream write resource name in its ``stdout_stream_name`` field. The worker will stream stdout and stderr merged in the same stream, so the output will be equivalent to the one observed if the command was executing in a local terminal.


Debugging/testing
-----------------
To enable running automated tests and help debugging the streaming mechanism, buildbox-worker provides an optional ``--launch-logstream-command`` CLI option to specify a command that will be executed before invoking the runner. Occurrences of ``{stdout}`` and ``{stderr}`` in the string get replaced with the respective resource names that were issued by the execution server (note that the names belong to the write end of the LogStreams, and they contain a token that is private to the worker).

The runner will not be invoked until the command returns.

Note that this option is only intended for testing purposes. To enable it, add options ``-DCMAKE_BUILD_TYPE=DEBUG -DLOGSTREAM_DEBUG=ON`` to cmake when building buildbox-worker.

.. _LogStream API: https://groups.google.com/g/remote-execution-apis/c/LCLsBSgSnU0/m/yTTtqr-cAwAJ
.. _trailing metadata: https://grpc.github.io/grpc/cpp/classgrpc__impl_1_1_client_context.html#a81d5df78eb77fb0d1e97220f656afd95
