#include <build/bazel/remote/execution/v2/remote_execution.grpc.pb.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <buildboxworker_executeactionutils.h>

using namespace buildboxworker;
TEST(ExecutionActionUtilsTests, TestSetEmptyBotID)
{
    const std::string some_bot("");
    ExecuteActionUtils::ExecutedActionMetadata metadata, defaultMetadata;
    ExecuteActionUtils::updateExecuteActionMetadataWorkerProperties(&metadata,
                                                                    some_bot);

    ASSERT_EQ(defaultMetadata.worker(), metadata.worker());
}

TEST(ExecutionActionUtilsTests, TestSetBotID)
{
    const std::string some_bot("abc");
    ExecuteActionUtils::ExecutedActionMetadata metadata;
    ExecuteActionUtils::updateExecuteActionMetadataWorkerProperties(&metadata,
                                                                    some_bot);

    ASSERT_EQ(some_bot, metadata.worker());
}
