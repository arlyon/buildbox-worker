/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_actionutils.h>
#include <buildboxworker_metricnames.h>
#include <buildboxworker_subprocessguard.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_client.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_systemutils.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_metricguard.h>

#include <fcntl.h>
#include <fstream>
#include <sys/wait.h>
#include <unistd.h>

namespace buildboxworker {

buildboxcommon::ActionResult
ActionUtils::readActionResultFile(const char *path)
{
    buildboxcommon::ActionResult actionResult;
    std::string actionResultRaw;
    bool success = false;

    { // Timed Block
        buildboxcommon::buildboxcommonmetrics::MetricGuard<
            buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
            mt(MetricNames::TIMER_NAME_READ_ACTION_RESULT);
        actionResultRaw = buildboxcommon::FileUtils::getFileContents(path);
        if (actionResultRaw.empty()) {
            // The ActionResult file is empty, and an empty ActionResult isn't
            // valid
            const std::string error_message =
                "ActionResultFile " + std::string(path) + " is an empty file";
            BUILDBOX_LOG_ERROR(error_message);
            BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error, error_message);
        }
        success = actionResult.ParseFromString(actionResultRaw);
    }

    if (!success) {
        const std::string error_message =
            "Unable to parse ActionResultFile " + std::string(path) +
            ": content of file was [" + actionResultRaw + "]";
        BUILDBOX_LOG_ERROR(error_message);
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error, error_message);
    }

    return actionResult;
}

/**
 * Start a subprocess with the given command and return its PID.
 *
 * The first element in `command` must be a path to an executable file.
 */
pid_t ActionUtils::runCommandInSubprocess(
    const std::vector<std::string> &command, Worker *worker)
{
    const pid_t pid = fork();

    if (pid == -1) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category, "error in fork()");
    }

    else if (pid == 0) {
        // (runs only in the child)
        const int devNullFD = open("/dev/null", O_RDWR);
        if (devNullFD == -1) {
            perror("/dev/null unavailable");
            _Exit(1);
        }

        // Set the group pid of the subprocess to its pid.
        if (setpgid(0, 0) != 0) {
            BUILDBOX_LOG_ERROR(
                "Unable to set group process id to subprocess, due to: "
                << strerror(errno));
            _Exit(1);
        }

        // Prevent worker processes from reading buildbox-worker's stdin
        dup2(devNullFD, STDIN_FILENO);
        close(devNullFD);

        const pid_t process_pid = getpid();
        BUILDBOX_LOG_DEBUG("I am the runner process: pid ["
                           << process_pid << "], group-pid ["
                           << getpgid(process_pid) << "]");

        const auto exit_code =
            buildboxcommon::SystemUtils::executeCommand(command);

        perror(nullptr);
        _Exit(exit_code);
    }

    else {
        // Set the group pid of the subprocess to its pid.
        // This is to prevent a race condition in which the child processes
        // pgid is checked in the parent before it is set in the child. To
        // prevent this, we also set it in the parent.
        if (setpgid(pid, 0) != 0) {
            // EACCES would be set if the child had exec'd. That is okay in
            // this case, as the pgid is set in the child as well.
            if (errno != EACCES) {
                BUILDBOX_LOG_ERROR(
                    "Unable to set group process id to subprocess, due to: "
                    << strerror(errno));
                worker->decrementThreadCount();
                pthread_exit(nullptr);
            }
        }
        return pid;
    }
}

int ActionUtils::executeActionInSubprocess(
    const std::vector<std::string> &command, Worker *worker)
{
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_EXECUTE_ACTION);

    BUILDBOX_LOG_DEBUG(
        "Launching runner process: "
        << buildboxcommon::logging::printableCommandLine(command));
    const auto subprocessPid = runCommandInSubprocess(command, worker);
    SubprocessGuard g(subprocessPid, worker);
    return waitForRunner(subprocessPid, worker);
}

// Wait for the runner subprocess to exit and return it's exit code
// throws a std::system_error exception if waitpid has an error
int ActionUtils::waitForRunner(pid_t subprocessPid, Worker *worker)
{
    int status = 0;
    while (true) { // Waiting until the process exits or get signaled...
        const pid_t pid = waitpid(subprocessPid, &status, 0);

        if (pid == -1) {
            const auto waitpid_error = errno;

            if (waitpid_error == EINTR) {
                continue; // Not an error, we can keep waiting.
            }
            BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
                std::system_error, errno, std::system_category,
                "Error while waiting for worker subprocess");
        }

        if (WIFSIGNALED(status)) {
            BUILDBOX_LOG_INFO("Worker received signal " << WSTOPSIG(status));
            worker->decrementThreadCount();
            pthread_exit(nullptr);
        }

        if (WIFEXITED(status)) {
            BUILDBOX_LOG_INFO("Worker exited with code "
                              << WEXITSTATUS(status));
            break;
        }
    }
    return status;
}

std::pair<buildboxcommon::Action, buildboxcommon::Digest>
ActionUtils::getActionFromLease(
    const proto::Lease &lease,
    const buildboxcommon::ConnectionOptions &casServerConnection)
{

    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_DOWNLOAD_ACTION);

    if (lease.payload().Is<buildboxcommon::Action>()) {
        buildboxcommon::Action action;
        lease.payload().UnpackTo(&action);

        const buildboxcommon::Digest digest =
            buildboxcommon::CASHash::hash(action.SerializeAsString());

        return std::make_pair(action, digest);
    }

    if (lease.payload().Is<buildboxcommon::Digest>()) {
        BUILDBOX_LOG_DEBUG("Payload is of type `Digest`, "
                           "fetching the corresponding "
                           "`Action` from CAS server");

        buildboxcommon::Digest digest;
        lease.payload().UnpackTo(&digest);

        const buildboxcommon::Action action =
            downloadAction(digest, casServerConnection);

        return std::make_pair(action, digest);
    }

    BUILDBOXCOMMON_THROW_EXCEPTION(
        std::runtime_error, "Lease contains unexpected payload type: `"
                                << lease.payload().GetTypeName()
                                << "` . (Expected `Action` or `Digest`.)");
}

buildboxcommon::Action ActionUtils::downloadAction(
    const buildboxcommon::Digest &digest,
    const buildboxcommon::ConnectionOptions &casServerConnection)
{
    try {
        buildboxcommon::Client client;
        client.init(casServerConnection);
        return client.fetchMessage<buildboxcommon::Action>(digest);
    }
    catch (const std::exception &e) {
        BUILDBOX_LOG_DEBUG("Failed to fetch digest from CAS server in \""
                           << casServerConnection.d_url << "\": " << e.what());
        throw;
    }
}

} // namespace buildboxworker
