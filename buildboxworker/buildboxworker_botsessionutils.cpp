/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_metricnames.h>

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_grpcretry.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_metricguard.h>

#include <buildboxworker_botsessionutils.h>

namespace buildboxworker {

using namespace buildboxcommon;

grpc::Status BotSessionUtils::updateBotSession(
    const proto::BotStatus botStatus,
    std::shared_ptr<proto::Bots::StubInterface> stub,
    const long requestTimeout,
    const buildboxcommon::ConnectionOptions &botsServerConnection,
    proto::BotSession *session,
    ExecuteOperationMetadataEntriesMultiMap *executeOperationMetadataEntries)
{
    // Set deadline on each request. If we don't, it defaults to a large
    // number.
    const auto deadlineSetter =
        [requestTimeout](grpc::ClientContext *context) {
            std::chrono::system_clock::time_point deadline =
                std::chrono::system_clock::now() +
                std::chrono::seconds(requestTimeout);
            context->set_deadline(deadline);
        };

    const auto updateInvocation = [&](grpc::ClientContext &context) {
        BUILDBOX_LOG_TRACE("Updating bot session (currently botstatus="
                           << BotStatus_Name(botStatus) << ")");
        proto::UpdateBotSessionRequest updateRequest;
        updateRequest.set_name(session->name());
        *updateRequest.mutable_bot_session() = *session;

        const auto status =
            stub->UpdateBotSession(&context, updateRequest, session);

        if (status.ok() && executeOperationMetadataEntries != nullptr) {
            *executeOperationMetadataEntries = extractExecuteOperationMetadata(
                context.GetServerTrailingMetadata());
        }

        return status;
    };

    grpc::Status status;
    session->set_status(botStatus);

    try { // Timed Block
        buildboxcommon::buildboxcommonmetrics::MetricGuard<
            buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
            mt(MetricNames::TIMER_NAME_UPDATE_BOTSESSION);
        int retryLimit = std::stoi(botsServerConnection.d_retryLimit);
        int retryDelay = std::stoi(botsServerConnection.d_retryDelay);
        GrpcRetry::retry(updateInvocation, "UpdateBotSession()", retryLimit,
                         retryDelay, deadlineSetter);
    }
    catch (const GrpcError &e) {
        BUILDBOX_LOG_ERROR(
            "Unable to update bot session, exiting: " << e.what());
        exit(1);
    }

    return status;
}

grpc::Status BotSessionUtils::createBotSession(
    std::shared_ptr<proto::Bots::StubInterface> stub,
    const buildboxcommon::ConnectionOptions &botsServerConnection,
    proto::BotSession *session)
{
    grpc::ClientContext context;
    proto::CreateBotSessionRequest createRequest;

    BUILDBOX_LOG_DEBUG("Setting parent");
    createRequest.set_parent(botsServerConnection.d_instanceName);
    *createRequest.mutable_bot_session() = *session;

    BUILDBOX_LOG_DEBUG("Setting session");

    grpc::Status status;
    { // Timed Block
        buildboxcommon::buildboxcommonmetrics::MetricGuard<
            buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
            mt(MetricNames::TIMER_NAME_CREATE_BOTSESSION);
        status = stub->CreateBotSession(&context, createRequest, session);
    }
    return status;
}

BotSessionUtils::ExecuteOperationMetadataEntriesMultiMap
BotSessionUtils::extractExecuteOperationMetadata(
    const GrpcServerMetadataMultiMap &metadata)
{
    const auto executeOperationMetadataName = "executeoperationmetadata-bin";

    ExecuteOperationMetadataEntriesMultiMap res;
    for (const auto &entry : metadata) {
        if (entry.first == executeOperationMetadataName) {
            ExecuteOperationMetadata message;
            if (message.ParseFromString(
                    std::string(entry.second.data(), entry.second.length()))) {
                res.emplace(message.action_digest(), message);
            }
        }
    }
    return res;
}

} // namespace buildboxworker
