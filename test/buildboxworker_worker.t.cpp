#include <gtest/gtest.h>

#include <google/devtools/remoteworkers/v1test2/bots_mock.grpc.pb.h>

#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_testingutils.h>
#include <buildboxworker_metricnames.h>
#include <buildboxworker_runnercommandutils.h>
#include <buildboxworker_worker.h>

#include <algorithm>

using buildboxcommon::buildboxcommonmetrics::DurationMetricValue;
using buildboxworker::Worker;
using google::devtools::remoteworkers::v1test2::MockBotsStub;

using namespace testing;

class WorkerTests : public ::testing::Test {
  protected:
    buildboxworker::Worker worker;
    WorkerTests() { worker.d_stub = std::make_unique<MockBotsStub>(); }
};

class WorkerTestFixture : public Worker, public ::testing::Test {
  protected:
    WorkerTestFixture() {}
};

TEST_F(WorkerTestFixture, DefaultWaitTime)
{
    ASSERT_FALSE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(), this->s_defaultWaitTime);
}

TEST_F(WorkerTestFixture, WaitTimeSaturates)
{
    const auto excessive_wait_time =
        std::chrono::duration_cast<std::chrono::seconds>(2 *
                                                         this->s_maxWaitTime);

    this->d_session.mutable_expire_time()->set_seconds(
        excessive_wait_time.count());

    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(), this->s_defaultWaitTime);
}

TEST_F(WorkerTestFixture, RunnerCommand)
{
    d_runnerCommand = "/bin/buildbox-run";
    // Specifying a full path because otherwise `Worker::buildRunnerCommand()`
    // will attempt to look up the command and throw due to it not existing,
    // while a path will remain unmodified.

    buildboxcommon::TemporaryFile actionFile;
    buildboxcommon::TemporaryFile actionResultFile;
    buildboxcommon::TemporaryFile stdoutFile;
    buildboxcommon::TemporaryFile stderrFile;

    const std::vector<std::string> runnerCommand = buildRunnerCommand(
        actionFile, actionResultFile, stdoutFile, stderrFile);

    std::vector<std::string> expectedCommand =
        buildboxworker::RunnerCommandUtils::buildRunnerCommand(
            d_runnerCommand, d_extraRunArgs, actionFile.strname(),
            actionResultFile.strname(), d_casServer, d_logLevel, d_logFile);
    buildboxworker::RunnerCommandUtils::setRunnerStderrStdoutFile(
        stdoutFile.strname(), stderrFile.strname(), &expectedCommand);

    ASSERT_TRUE(std::equal(runnerCommand.cbegin(), runnerCommand.cend(),
                           expectedCommand.cbegin(), expectedCommand.cend()));
}
