#include <gtest/gtest.h>

#include <google/devtools/remoteworkers/v1test2/bots_mock.grpc.pb.h>

#include <buildboxworker_botsessionutils.h>

#include <buildboxcommon_protos.h>
#include <buildboxcommonmetrics_durationmetricvalue.h>
#include <buildboxcommonmetrics_testingutils.h>
#include <buildboxworker_metricnames.h>

using namespace testing;
using namespace buildboxworker::proto;
using buildboxcommon::buildboxcommonmetrics::collectedByName;
using buildboxcommon::buildboxcommonmetrics::DurationMetricValue;
using buildboxworker::BotSessionUtils;

using google::devtools::remoteworkers::v1test2::MockBotsStub;

class BotSessionUtilsTests : public ::testing::Test {
  protected:
    BotStatus botStatus = BotStatus::OK;
    std::shared_ptr<Bots::StubInterface> stub;
    long requestTimeout = 1.0;
    BotSession botSession;
    buildboxcommon::ConnectionOptions botsServerConnection;
    BotSessionUtilsTests() { stub = std::make_shared<MockBotsStub>(); }
};

/*
 * UpdateBotSession succeeds on the first try.
 */
TEST_F(BotSessionUtilsTests, UpdateBotSessionHappyPath)
{
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), UpdateBotSession(_, _, _))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::updateBotSession(botStatus, stub, requestTimeout,
                                      botsServerConnection, &botSession);
}

/*
 * UpdateBotSession succeeds after one retry.
 */
TEST_F(BotSessionUtilsTests, UpdateBotSessionEventualSuccess)
{
    botsServerConnection.d_retryLimit = "1";
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), UpdateBotSession(_, _, _))
        .WillOnce(Return(grpc::Status(grpc::UNAVAILABLE, "Failing for test")))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::updateBotSession(botStatus, stub, requestTimeout,
                                      botsServerConnection, &botSession);
}

/*
 * Verify that metrics are collected for UpdateBotSession.
 */
TEST_F(BotSessionUtilsTests, UpdateBotSessionMetricsCollected)
{
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), UpdateBotSession(_, _, _))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::updateBotSession(botStatus, stub, requestTimeout,
                                      botsServerConnection, &botSession);
    ASSERT_TRUE(collectedByName<DurationMetricValue>(
        buildboxworker::MetricNames::TIMER_NAME_UPDATE_BOTSESSION));
}

class CreateBotSessionTests : public BotSessionUtilsTests {
  protected:
    CreateBotSessionTests()
    {
        botSession.set_bot_id("foo");
        botsServerConnection.d_instanceName = "dev";
    }
};

TEST_F(CreateBotSessionTests, CreateBotSessionHappyPath)
{
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), CreateBotSession(_, _, _))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::createBotSession(stub, botsServerConnection, &botSession);
}

TEST_F(CreateBotSessionTests, CreateBotSessionMetricsCollected)
{
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), CreateBotSession(_, _, _))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::createBotSession(stub, botsServerConnection, &botSession);
    ASSERT_TRUE(collectedByName<DurationMetricValue>(
        buildboxworker::MetricNames::TIMER_NAME_CREATE_BOTSESSION));
}

TEST(BotSessionUtilsTest, ExtractValidExecuteOperationMetadata)
{
    buildboxcommon::Digest actionDigest;
    actionDigest.set_hash("hash");
    actionDigest.set_size_bytes(123);

    const auto streamResourceName = "stream/outputs/here";
    buildboxcommon::ExecuteOperationMetadata metadata;
    metadata.set_stdout_stream_name(streamResourceName);
    metadata.mutable_action_digest()->CopyFrom(actionDigest);

    const std::string serializedMetadata = metadata.SerializeAsString();

    const auto metadataName = "executeoperationmetadata-bin";
    const std::multimap<grpc::string_ref, grpc::string_ref> metadataMultiMap =
        {{metadataName, serializedMetadata}, {"val", "123"}};

    const BotSessionUtils::ExecuteOperationMetadataEntriesMultiMap
        extractedMetadata =
            BotSessionUtils::extractExecuteOperationMetadata(metadataMultiMap);

    ASSERT_EQ(extractedMetadata.size(), 1);
    const auto extractedEntry = extractedMetadata.find(actionDigest)->second;
    ASSERT_EQ(extractedEntry.stdout_stream_name(),
              metadata.stdout_stream_name());
    ASSERT_EQ(extractedEntry.action_digest(), actionDigest);
}

TEST(BotSessionUtilsTest, ExtractEmptyExecuteOperationMetadata)
{
    const std::multimap<grpc::string_ref, grpc::string_ref> metadataMultiMap =
        {{"foo", "bar"}, {"a", "b"}};

    ASSERT_TRUE(
        BotSessionUtils::extractExecuteOperationMetadata(metadataMultiMap)
            .empty());
}
