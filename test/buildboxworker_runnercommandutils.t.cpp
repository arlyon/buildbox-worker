#include <gtest/gtest.h>

#include <buildboxworker_runnercommandutils.h>

TEST(RunnerCommandUtils, TestBuildRunnerCommand)
{
    auto casConnection = buildboxcommon::ConnectionOptions();
    casConnection.d_url = "http://cas:50001";
    casConnection.d_instanceName = "cas-instance";

    const std::vector<std::string> command =
        buildboxworker::RunnerCommandUtils::buildRunnerCommand(
            "/bin/runner", {"--runner-extra-arg=yes"}, "/path/to/actionFile",
            "/path/to/actionResultFile", casConnection, "verbose",
            "/tmp/runner.log");

    std::vector<std::string> expectedCommand = {
        "/bin/runner",
        "--action=/path/to/actionFile",
        "--action-result=/path/to/actionResultFile",
        "--log-level=verbose",
        "--log-file=/tmp/runner.log",
        "--runner-extra-arg=yes"};
    casConnection.putArgs(&expectedCommand);

    // Since there no positional arguments, other than the path to the binary
    // being at the beginning, we don't care about the actual order in the
    // result:
    ASSERT_EQ(command.at(0), "/bin/runner");
    ASSERT_TRUE(std::is_permutation(expectedCommand.cbegin() + 1,
                                    expectedCommand.cend(),
                                    command.cbegin() + 1));
}

TEST(RunnerCommandUtils, TestBuildRunnerCommandNoExtraArgs)
{
    auto casConnection = buildboxcommon::ConnectionOptions();
    casConnection.d_url = "http://cas:50001";
    casConnection.d_instanceName = "cas-instance";

    const std::vector<std::string> command =
        buildboxworker::RunnerCommandUtils::buildRunnerCommand(
            "/bin/runner", {}, "/path/to/actionFile",
            "/path/to/actionResultFile", casConnection, "verbose", "");

    std::vector<std::string> expectedCommand = {
        "/bin/runner", "--action=/path/to/actionFile",
        "--action-result=/path/to/actionResultFile"};
    casConnection.putArgs(&expectedCommand);

    // Since there no positional arguments, other than the path to the binary
    // being at the beginning, we don't care about the actual order in the
    // result:
    ASSERT_EQ(command.at(0), "/bin/runner");
    ASSERT_TRUE(std::is_permutation(expectedCommand.cbegin() + 1,
                                    expectedCommand.cend(),
                                    command.cbegin() + 1));
}

TEST(RunnerCommandUtils, TestBuildRunnerCommandStream)
{
    const std::string stdoutPath = "/path/to/stdout.log";
    const std::string stderrPath = "/path/to/stderr.log";

    std::vector<std::string> command = {"/path/to/runner", "--foo",
                                        "--option=true"};
    buildboxworker::RunnerCommandUtils::setRunnerStderrStdoutFile(
        stdoutPath, stderrPath, &command);

    const std::vector<std::string> expectedResult = {
        "/path/to/runner", "--foo", "--option=true",
        "--stdout-file=" + stdoutPath, "--stderr-file=" + stderrPath};

    ASSERT_TRUE(
        std::equal(command.cbegin(), command.cend(), expectedResult.cbegin()));
}

TEST(RunnerCommandUtils, TestBuildRunnerCommandStreamNullptr)
{

    ASSERT_THROW(buildboxworker::RunnerCommandUtils::setRunnerStderrStdoutFile(
                     "pathErr", "pathOut", nullptr),
                 std::runtime_error);
}
