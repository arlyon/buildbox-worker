/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_worker.h>

#include <buildboxworker_actionutils.h>
#include <buildboxworker_botsessionutils.h>
#include <buildboxworker_config.h>
#include <buildboxworker_expiretime.h>
#include <buildboxworker_logstreamdebugutils.h>
#include <buildboxworker_metricnames.h>
#include <buildboxworker_runnercommandutils.h>

#include <buildboxcommon_client.h>
#include <buildboxcommon_connectionoptions_commandline.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_grpcretry.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_systemutils.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_metricguard.h>
#include <buildboxcommonmetrics_metricsconfigurator.h>
#include <buildboxworker_executeactionutils.h>

#include <google/devtools/remoteworkers/v1test2/worker.grpc.pb.h>
#include <google/rpc/code.pb.h>
#include <grpcpp/channel.h>

#include <csignal>
#include <cstring>
#include <exception>
#include <fcntl.h>
#include <iomanip>
#include <pthread.h>
#include <signal.h>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <system_error>
#include <thread>
#include <unistd.h>

namespace buildboxworker {

const std::chrono::microseconds
    Worker::s_defaultWaitTime(std::chrono::milliseconds(250));

const std::chrono::microseconds
    Worker::s_maxWaitTime(std::chrono::minutes(10));

namespace {

std::string generateDefaultBotID()
{
    char hostname[256] = {0};
    gethostname(hostname, sizeof(hostname));
    return std::string(hostname) + "-" + std::to_string(getpid());
}

/**
 * string-ifys a std::chrono::time_point to a human-readable time in
 * microsecond res
 */
std::string timePointToStr(const std::chrono::system_clock::time_point &tp)
{
    const time_t nowAsTimeT = std::chrono::system_clock::to_time_t(tp);
    const std::chrono::microseconds micros =
        std::chrono::duration_cast<std::chrono::microseconds>(
            tp.time_since_epoch()) %
        static_cast<int>(1e6);
    struct tm localtime;
    localtime_r(&nowAsTimeT, &localtime);

    std::ostringstream os;
    os << std::put_time(&localtime, "%FT%T") << '.' << std::setfill('0')
       << std::setw(3) << micros.count() << std::put_time(&localtime, "%z");

    return os.str();
}

/**
 * Convenience functions for determining which type of signal was just received
 */
bool rereadConfigRequested(const int signal) { return (signal == SIGHUP); }

bool shutdownRequested(const int signal)
{
    return (signal == SIGINT || signal == SIGTERM);
}

volatile std::sig_atomic_t k_signalStatus;

} // namespace

void signal_handler(int signal) { k_signalStatus = signal; }

Worker::Worker(
    const buildboxcommon::CommandLine &cml, const std::string &botId,
    buildboxcommon::buildboxcommonmetrics::MetricsConfigType *metricsConfig)
    : d_botID(!botId.empty() ? botId : generateDefaultBotID()),
      d_instanceName(cml.getString("instance")),
      d_maxConcurrentJobs(cml.getInt("concurrent-jobs")),
      d_stopAfterJobs(cml.getInt("stop-after")),
      d_runnerCommand(cml.getString("buildbox-run")),
      d_extraRunArgs(cml.getVS("runner-arg")),
      d_requestTimeout(cml.getInt("request-timeout")),
      d_configFileName(cml.getString("config-file")),
      d_logLevel(cml.getString("log-level")),
      d_logFile(cml.getString("log-file")),
      d_botStatus(Config::getStatusFromConfigFile(d_configFileName))
{
    if (cml.exists("platform")) {
        d_platform = cml.getVPS("platform");
    }

    if (buildboxcommon::logging::stringToLogLevel.find(d_logLevel) ==
        buildboxcommon::logging::stringToLogLevel.end()) {
        BUILDBOX_LOG_ERROR("Invalid log level \"" << d_logLevel
                                                  << "\" setting to INFO");
        d_logLevel = "info";
        BUILDBOX_LOG_SET_LEVEL(buildboxcommon::LogLevel::INFO);
    }
    else if (cml.exists("verbose")) {
        BUILDBOX_LOG_SET_LEVEL(buildboxcommon::LogLevel::DEBUG);
    }
    else {
        BUILDBOX_LOG_SET_LEVEL(
            buildboxcommon::logging::stringToLogLevel.at(d_logLevel));
    }

    if (!d_logFile.empty()) {
        const std::string &fileName = cml.getString("log-file");
        FILE *fp = fopen(fileName.c_str(), "w");
        if (fp == nullptr) {
            std::ostringstream oss;
            oss << "--log-file: unable to write to \"" << fileName << "\"";
            BUILDBOX_LOG_ERROR(oss.str());
            throw std::runtime_error(oss.str());
        }
        fclose(fp);
        BUILDBOX_LOG_SET_FILE(d_logFile);
    }

    if (cml.exists("metrics-mode")) {
        buildboxcommon::buildboxcommonmetrics::MetricsConfigurator::
            metricsParser("metrics-mode", cml.getString("metrics-mode"),
                          metricsConfig);
    }

    if (cml.exists("metrics-publish-interval")) {
        buildboxcommon::buildboxcommonmetrics::MetricsConfigurator::
            metricsParser(
                "metrics-publish-interval",
                std::to_string(cml.getInt("metrics-publish-interval")),
                metricsConfig);
    }

    // Configuring the bots, CAS and LogStream (optional) channels:
    buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        cml, "bots-", &d_botsServer);

    buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        cml, "cas-", &d_casServer);

    buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        cml, "logstream-", &d_logStreamServer);

    // Backwards-compatible for scenarios where older startup scripts didn't
    // explicitly pass in '--cas-instance' or '--bots-instance' CLI

    // this is required as per the spec
    if (cml.getString("bots-instance").empty()) {
        d_botsServer.setInstanceName(d_instanceName);
    }
    // this is required as per the spec
    if (cml.getString("cas-instance").empty()) {
        d_casServer.setInstanceName(d_instanceName);
    }
    // this is optional as per the spec
    if (cml.exists("logstream-instance") &&
        cml.getString("logstream-instance").empty()) {
        d_logStreamServer.setInstanceName(d_instanceName);
    }

#ifdef LOGSTREAM_DEBUG
    d_logstreamDebugCommand = cml.getString("launch-logstream-command", "");
#endif
}

bool Worker::validateConfiguration()
{
    if (d_botsServer.d_url == nullptr) {
        BUILDBOX_LOG_ERROR("Bots server URL is missing");
        return false;
    }

    if (d_casServer.d_url == nullptr) {
        BUILDBOX_LOG_ERROR("CAS server URL is missing");
        return false;
    }

    if (d_stopAfterJobs > 0 && d_maxConcurrentJobs > d_stopAfterJobs) {
        BUILDBOX_LOG_INFO(
            "WARNING: Max concurrent jobs ("
            << d_maxConcurrentJobs << ") > number of jobs to stop after ("
            << d_stopAfterJobs << "). Capping concurrent jobs to "
            << d_stopAfterJobs << ".");
        d_maxConcurrentJobs = d_stopAfterJobs;
    }

    return true;
}

std::vector<std::string>
Worker::buildRunnerCommand(buildboxcommon::TemporaryFile &actionFile,
                           buildboxcommon::TemporaryFile &actionResultFile,
                           buildboxcommon::TemporaryFile &stdoutFile,
                           buildboxcommon::TemporaryFile &stderrFile)
{

    // `ActionUtils::executeActionInSubprocess()` needs the full path to the
    // runner binary.
    const auto path_to_runner =
        buildboxcommon::SystemUtils::getPathToCommand(this->d_runnerCommand);
    if (path_to_runner.empty()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Could not find runner command \"" +
                                           this->d_runnerCommand + "\"");
    }

    std::vector<std::string> command = RunnerCommandUtils::buildRunnerCommand(
        path_to_runner, d_extraRunArgs, actionFile.strname(),
        actionResultFile.strname(), d_casServer, d_logLevel, d_logFile);

    RunnerCommandUtils::setRunnerStderrStdoutFile(
        stdoutFile.strname(), stderrFile.strname(), &command);

    return command;
}

void Worker::setUpStreamingIfNeeded(
    const std::string &stdoutStream, const std::string &stdoutFilePath,
    const std::string &stderrStream, const std::string &stderrFilePath,
    std::unique_ptr<buildboxcommon::StandardOutputStreamer> *stdoutStreamer,
    std::unique_ptr<buildboxcommon::StandardOutputStreamer> *stderrStreamer)
{
    if (!this->d_logStreamServer.d_url) {
        return;
    }

    if (!stdoutStream.empty()) {
        BUILDBOX_LOG_INFO("Reading runner stdout from [ "
                          << stdoutFilePath << "] and streaming it to ["
                          << stdoutStream << "]");

        *stdoutStreamer =
            std::make_unique<buildboxcommon::StandardOutputStreamer>(
                stdoutFilePath, this->d_logStreamServer, stdoutStream);
    }

    if (!stderrStream.empty()) {
        BUILDBOX_LOG_INFO("Reading runner stderr from [ "
                          << stderrFilePath << "] and streaming it to ["
                          << stderrStream << "]");

        *stderrStreamer =
            std::make_unique<buildboxcommon::StandardOutputStreamer>(
                stderrFilePath, this->d_logStreamServer, stderrStream);
    }

#ifdef LOGSTREAM_DEBUG
    LogStreamDebugUtils::launchDebugCommand(d_logstreamDebugCommand,
                                            stdoutStream, stderrStream);
#endif
}

void Worker::stopStreamingIfNeeded(
    const std::unique_ptr<buildboxcommon::StandardOutputStreamer>
        &stdoutStreamer,
    const std::unique_ptr<buildboxcommon::StandardOutputStreamer>
        &stderrStreamer)
{
    if (stdoutStreamer) {
        if (stdoutStreamer->stop()) {
            BUILDBOX_LOG_DEBUG("stdout successfully committed");
        }
        else {
            BUILDBOX_LOG_ERROR(
                "Streaming operation failed, stdout was not committed");
        }
    }

    if (stderrStreamer) {
        if (stderrStreamer->stop()) {
            BUILDBOX_LOG_DEBUG("stderr successfully committed");
        }
        else {
            BUILDBOX_LOG_ERROR(
                "Streaming operation failed, stderr was not committed");
        }
    }
}

buildboxcommon::ActionResult
Worker::executeAction(buildboxcommon::TemporaryFile &actionFile,
                      const std::string &stdoutStream,
                      const std::string &stderrStream)
{
    buildboxcommon::TemporaryFile actionResultFile;
    actionFile.close();
    actionResultFile.close();

    buildboxcommon::TemporaryFile stdoutFile;
    stdoutFile.close();
    buildboxcommon::TemporaryFile stderrFile;
    stderrFile.close();

    const std::vector<std::string> command = buildRunnerCommand(
        actionFile, actionResultFile, stdoutFile, stderrFile);

    std::unique_ptr<buildboxcommon::StandardOutputStreamer> stdoutStreamer;
    std::unique_ptr<buildboxcommon::StandardOutputStreamer> stderrStreamer;
    setUpStreamingIfNeeded(stdoutStream, stdoutFile.strname(), stderrStream,
                           stderrFile.strname(), &stdoutStreamer,
                           &stderrStreamer);

    const int rc = ActionUtils::executeActionInSubprocess(command, this);

    stopStreamingIfNeeded(stdoutStreamer, stderrStreamer);

    if (shutdownRequested(k_signalStatus)) {
        decrementThreadCount();
        pthread_exit(nullptr);
    }
    if (rc != 0) {
        BUILDBOX_LOG_WARNING("Runner exited with non-zero exit code [" << rc
                                                                       << "]");
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Runner exited with non-zero exit code \"" << rc << "\"");
    }

    const buildboxcommon::ActionResult actionResult =
        ActionUtils::readActionResultFile(actionResultFile.name());
    BUILDBOX_LOG_DEBUG("ActionResult " << actionResult.DebugString());

    return actionResult;
}

void Worker::workerThread(const std::string &leaseId)
{
    BUILDBOX_LOG_DEBUG("Starting worker thread for leaseId " << leaseId);

    if (shutdownRequested(k_signalStatus)) {
        decrementThreadCount();
        return;
    }

    buildboxcommon::ActionResult result;

    google::rpc::Status leaseStatus;
    leaseStatus.set_code(google::rpc::Code::OK);
    try {
        buildboxcommon::TemporaryFile actionFile;
        std::string stdoutStream;
        std::string stderrStream;

        { // Lock while fetching the action
            std::lock_guard<std::mutex> lock(this->d_sessionMutex);

            // Searching for the lease...
            const auto lease = std::find_if(d_session.leases().cbegin(),
                                            d_session.leases().cend(),
                                            [&leaseId](const proto::Lease &l) {
                                                return l.id() == leaseId;
                                            });
            if (lease == d_session.leases().cend()) { // Not found!
                --this->d_detachedThreadCount;
                this->d_sessionCondition.notify_all();
                return;
            }

            // We found it, reading its action:
            buildboxcommon::Action action;
            buildboxcommon::Digest actionDigest;
            std::tie(action, actionDigest) =
                ActionUtils::getActionFromLease(*lease, d_casServer);

            BUILDBOX_LOG_DEBUG("Saving Action to [" << actionFile.name()
                                                    << "]");
            if (!action.SerializeToFileDescriptor(actionFile.fd())) {
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::runtime_error, "Error writing Action with digest ["
                                            << actionDigest << "] to ["
                                            << actionFile.name() << "]");
            }

            // Fetching the optional stream names that can be set in metadata:
            std::tie(stdoutStream, stderrStream) =
                standardOutputsStreamNames(actionDigest);
        }

        result = executeAction(actionFile, stdoutStream, stderrStream);
        ExecuteActionUtils::updateExecuteActionMetadataWorkerProperties(
            result.mutable_execution_metadata(), d_botID);
    }
    catch (const std::exception &e) {
        BUILDBOX_LOG_ERROR("Caught exception in worker: " << e.what());
        leaseStatus.set_code(google::rpc::Code::ABORTED);
        leaseStatus.set_message(
            "System error when attempting to execute Action: [" +
            std::string(e.what()) + "]");
    }

    {
        // Store the ActionResult back in the lease.
        //
        // We need to search for the lease again because it could have been
        // moved/cancelled/deleted/completed by someone else while we were
        // executing it.
        std::lock_guard<std::mutex> lock(this->d_sessionMutex);
        for (auto &lease : *this->d_session.mutable_leases()) {
            if (lease.id() == leaseId &&
                lease.state() == proto::LeaseState::ACTIVE) {
                lease.set_state(proto::LeaseState::COMPLETED);
                lease.mutable_status()->CopyFrom(leaseStatus);
                if (leaseStatus.code() == google::rpc::Code::OK) {
                    lease.mutable_result()->PackFrom(result);
                }
            }
        }
        this->d_activeJobs.erase(leaseId);
        --this->d_detachedThreadCount;
    }
    this->d_sessionCondition.notify_all();
}

void Worker::registerSignals()
{
    struct sigaction sa;
    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;

    if (sigaction(SIGINT, &sa, nullptr) == -1) {
        BUILDBOX_LOG_INFO("Unable to register signal handler for SIGINT");
        exit(1);
    }
    if (sigaction(SIGTERM, &sa, nullptr) == -1) {
        BUILDBOX_LOG_INFO("Unable to register signal handler for SIGTERM");
        exit(1);
    }
    if (sigaction(SIGHUP, &sa, nullptr) == -1) {
        BUILDBOX_LOG_INFO("Unable to register signal handler for SIGHUP");
        exit(1);
    }
}

void Worker::setPlatformProperties()
{
    auto workerProto = this->d_session.mutable_worker();
    auto device = workerProto->add_devices();
    device->set_handle(this->d_botID);

    for (const auto &platformPair : this->d_platform) {
        // TODO Differentiate worker properties and device properties?
        auto workerProperty = workerProto->add_properties();
        workerProperty->set_key(platformPair.first);
        workerProperty->set_value(platformPair.second);

        auto deviceProperty = device->add_properties();
        deviceProperty->set_key(platformPair.first);
        deviceProperty->set_value(platformPair.second);
    }
}

bool Worker::hasJobsToProcess() const
{
    return (this->d_stopAfterJobs != 0 || this->d_activeJobs.size() > 0 ||
            this->d_jobsPendingAck.size() > 0);
}

void Worker::processLeases(bool *skipPollDelay)
{
    for (auto &lease : *this->d_session.mutable_leases()) {
        if (lease.state() == proto::LeaseState::PENDING) {
            processPendingLease(&lease, skipPollDelay);
        }
        else if (lease.state() == proto::LeaseState::ACTIVE) {
            processActiveLease(lease);
        }
        else if (lease.state() == proto::LeaseState::CANCELLED) {
            processCancelledLease(lease);
        }
    }
}

void Worker::processPendingLease(proto::Lease *lease, bool *skipPollDelay)
{
    BUILDBOX_LOG_DEBUG("Processing pending lease: " << lease->DebugString());
    if (lease->payload().Is<buildboxcommon::Action>() ||
        lease->payload().Is<buildboxcommon::Digest>()) {

        if (static_cast<int>(this->d_activeJobs.size() +
                             this->d_jobsPendingAck.size()) <
                this->d_maxConcurrentJobs &&
            this->d_stopAfterJobs != 0) {
            // Accept the lease, but wait for the server's ack
            // before actually starting work on it.
            lease->set_state(proto::LeaseState::ACTIVE);
            *skipPollDelay = true;
            this->d_jobsPendingAck.insert(lease->id());

            if (this->d_stopAfterJobs > 0) {
                this->d_stopAfterJobs--;
            }
        }
    }
    else {
        BUILDBOX_LOG_ERROR("Lease payload is of an unexpected type (not "
                           "`Action` or `Digest`): "
                           << lease->DebugString());

        auto status = lease->mutable_status();
        status->set_message("Invalid lease");
        status->set_code(google::rpc::Code::INVALID_ARGUMENT);

        lease->set_state(proto::LeaseState::COMPLETED);
    }
}

void Worker::processActiveLease(const proto::Lease &lease)
{
    BUILDBOX_LOG_DEBUG("Processing active lease: " << lease.DebugString());

    this->d_jobsPendingAck.erase(lease.id());

    if (this->d_activeJobs.count(lease.id()) == 0) {
        const std::string leaseId = lease.id();
        auto thread =
            std::thread([this, leaseId] { this->workerThread(leaseId); });
        ++this->d_detachedThreadCount;
        thread.detach();
        // (the thread will remove its job from activeJobs when
        // it's done)
        this->d_activeJobs.insert(lease.id());
    }
}

void Worker::processCancelledLease(const proto::Lease &lease)
{
    BUILDBOX_LOG_DEBUG("Processing cancelled lease: " << lease.DebugString());

    if (this->d_jobsPendingAck.count(lease.id()) != 0) {
        // We accepted the job, but the server decided that we
        // shouldn't run it after all.
        this->d_jobsPendingAck.erase(lease.id());
        if (this->d_stopAfterJobs >= 0) {
            this->d_stopAfterJobs++;
        }
    }
}

std::chrono::microseconds Worker::calculateWaitTime() const
{
    if (!this->d_session.has_expire_time() || this->d_activeJobs.size() == 0) {
        // `expire_time` may be way too long if we don't have any pending
        // work, and that would mean workers won't pick up work for a
        // while. So in that case we use the default value.
        return s_defaultWaitTime;
    }

    // convert google.protobuf.Timestamp to std::chrono::time_point
    const auto expireTime =
        ExpireTime::convertToTimePoint(this->d_session.expire_time());

    // get current time
    const auto now = std::chrono::system_clock::now();

    if (expireTime <= now) {
        BUILDBOX_LOG_WARNING(
            "BotSession::expire_time is either "
            "now or in the past: expireTime = "
            << timePointToStr(expireTime) << ", now = " << timePointToStr(now)
            << ", using default wait time of "
            << std::chrono::duration_cast<std::chrono::seconds>(
                   s_defaultWaitTime)
                   .count()
            << " seconds");

        return s_defaultWaitTime;
    }

    // calc the difference
    auto waitTime = std::chrono::duration_cast<std::chrono::microseconds>(
        expireTime - now);

    // minus some percentage of time allow us to respond
    const auto factor = static_cast<int64_t>(
        waitTime.count() * ExpireTime::updateTimeoutPaddingFactor());
    waitTime -= std::chrono::microseconds(factor);

    // check for sane upper bounds
    if (waitTime > s_maxWaitTime) {
        BUILDBOX_LOG_WARNING(
            "detected excessive BotSession::expire_time set to "
            << waitTime.count() << "us, using max wait time of "
            << std::chrono::duration_cast<std::chrono::seconds>(s_maxWaitTime)
                   .count()
            << " seconds");

        waitTime = s_maxWaitTime;
    }

    return waitTime;
}

void Worker::decrementThreadCount()
{
    std::lock_guard<std::mutex> lock(this->d_sessionMutex);
    --this->d_detachedThreadCount;
    this->d_sessionCondition.notify_all();
}

void Worker::cleanupAfterUpdateBotSession(grpc::Status updateStatus,
                                          bool shouldExit)
{
    if (!updateStatus.ok()) {
        std::string message = "Failed to update bot status: gRPC error " +
                              to_string(updateStatus.error_code()) + ": " +
                              updateStatus.error_message();
        // grpcRetry will have logged the error for us.
        if (shouldExit) {
            std::lock_guard<std::mutex> lock(d_sessionMutex);
            BUILDBOX_LOG_ERROR("Unable to update bot session. Exiting, and "
                               "killing: ["
                               << d_subprocessPgid.size()
                               << "] subprocesses.");
            for (const auto &gpid : d_subprocessPgid) {
                if (killpg(gpid, SIGTERM) != 0) {
                    BUILDBOX_LOG_INFO("Sending signal to group pid: ["
                                      << gpid << "].");
                    // Continue if no process group id is found
                    if (errno != ESRCH) {
                        BUILDBOX_LOG_ERROR("Unable to send SIGTERM to "
                                           "processes with group pid: ["
                                           << gpid << "]. Due to: "
                                           << strerror(errno));
                        exit(1);
                    }
                }
            }
        }
    }
}

void Worker::trackSubprocess(const pid_t subprocessPid)
{
    std::lock_guard<std::mutex> lock(this->d_sessionMutex);
    d_subprocessPgid.insert(subprocessPid);
}

void Worker::untrackSubprocess(const pid_t subprocessPid)
{
    std::lock_guard<std::mutex> lock(this->d_sessionMutex);
    d_subprocessPgid.erase(subprocessPid);
}

std::pair<std::string, std::string> Worker::standardOutputsStreamNames(
    const buildboxcommon::Digest &actionDigest) const
{
    std::string stdoutStreamName;
    std::string stderrStreamName;

    const auto it = d_sessionExecuteOperationMetadata.find(actionDigest);
    if (it != d_sessionExecuteOperationMetadata.cend()) {
        stdoutStreamName = it->second.stdout_stream_name();
        stderrStreamName = it->second.stderr_stream_name();
    }

    return std::make_pair(stdoutStreamName, stderrStreamName);
}

void Worker::logSuppliedParameters() const
{
    std::ostringstream oss;
    oss << "Starting buildbox-worker with BotID = \"" << d_botID
        << "\", instanceName = \"" << d_instanceName << "\", CAS-remote = ["
        << d_casServer << "], BOTS-remote = [" << d_botsServer
        << "], log-level = \"" << d_logLevel
        << "\", request-timeout = " << d_requestTimeout
        << ", buildbox-run = \"" << d_runnerCommand << "\"";

    oss << ", runner-arg = [";
    for (size_t i = 0; i < d_extraRunArgs.size(); ++i) {
        if (i > 0) {
            oss << ", ";
        }
        oss << "\"" << d_extraRunArgs[i] << "\"";
    }

    oss << "], platform = [";
    for (size_t i = 0; i < d_platform.size(); ++i) {
        if (i > 0) {
            oss << ", ";
        }
        oss << "[\"" << d_platform[i].first << "\" = \""
            << d_platform[i].second << "\"]";
    }

    oss << "], config-file = \"" << d_configFileName
        << "\", botStatus = " << d_botStatus;

    BUILDBOX_LOG_INFO(oss.str());
}

void Worker::runWorker()
{
    registerSignals(); // Handle SIGINT, SIGTERM and SIGHUP

    logSuppliedParameters();

    this->d_session.set_bot_id(this->d_botID);
    setPlatformProperties();

    this->d_stub =
        std::move(proto::Bots::NewStub(this->d_botsServer.createChannel()));

    const auto createSessionStatus =
        BotSessionUtils::createBotSession(d_stub, d_botsServer, &d_session);
    if (!createSessionStatus.ok()) {
        BUILDBOX_LOG_ERROR("Failed to create bot session: gRPC error " +
                           to_string(createSessionStatus.error_code()) + ": " +
                           createSessionStatus.error_message());
        exit(1);
    }

    BUILDBOX_LOG_DEBUG("Bot Session created. Now waiting for jobs.");

    bool skipPollDelay = true;

    while (this->hasJobsToProcess() && !shutdownRequested(k_signalStatus)) {
        std::unique_lock<std::mutex> lock(this->d_sessionMutex);
        if (skipPollDelay) {
            skipPollDelay = false;
        }
        else {
            this->d_sessionCondition.wait_for(lock, this->calculateWaitTime());
        }

        if (rereadConfigRequested(k_signalStatus)) {
            d_botStatus =
                Config::getStatusFromConfigFile(this->d_configFileName);
            k_signalStatus = 0;
        }

        processLeases(&skipPollDelay);
        grpc::Status updateStatus = BotSessionUtils::updateBotSession(
            d_botStatus, d_stub, d_requestTimeout, d_botsServer, &d_session,
            &d_sessionExecuteOperationMetadata);

        cleanupAfterUpdateBotSession(updateStatus);
    }

    if (shutdownRequested(k_signalStatus)) {
        BUILDBOX_LOG_DEBUG("Received signal, exiting.");
        std::unique_lock<std::mutex> lock(this->d_sessionMutex);
        // Wait for all detached threads to finish.
        while (this->d_detachedThreadCount > 0) {
            this->d_sessionCondition.wait(lock);
            // Don't exit immediately when signal is being handled.
            grpc::Status updateStatus = BotSessionUtils::updateBotSession(
                proto::BotStatus::BOT_TERMINATING, d_stub, d_requestTimeout,
                d_botsServer, &d_session);

            cleanupAfterUpdateBotSession(updateStatus, false);
        }
        exit(1);
    }
}
} // namespace buildboxworker
