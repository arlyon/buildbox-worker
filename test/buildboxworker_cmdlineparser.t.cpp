#include <buildboxcommon_commandline.h>
#include <buildboxworker_cmdlinespec.h>
#include <buildboxworker_worker.h>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace buildboxworker;
using namespace testing;

// clang-format off
const char *argvTest[] = {
    "/some/path/to/some_program.tsk",
    "--instance=dev",
    "--cas-remote=http://127.0.0.1:50011",
    "--bots-remote=http://100.70.37.178:50051",
    "--log-level=debug",
    "--request-timeout=3",
    "--buildbox-run=/opt/remoteexecution/buildbox-run-hosttools",
    "--bots-retry-limit=4",
    "--bots-retry-delay=1000",
    "--cas-retry-limit=4",
    "--cas-retry-delay=1000",
    "--runner-arg=--prefix-staged-dir",
    "--runner-arg=--use-localcas",
    "--runner-arg=--userchroot-bin=/bb/dbldroot/bin/userchroot",
    "--platform",
    "OSFamily=linux",
    "--platform",
    "ISA=x86-64",
    "--platform",
    "chrootRootDigest=8533ec9ba7494cc8295ccd0bfdca08457421a28b4e92c8eb18e7178fb400f5d4/930",
    "--platform",
    "chrootRootDigest=1e7088e7aca9e8713a84122218a89c8908b39b5797d32170f1afa6e474b9ade6/930",
    "--config-file=/dbldroot/data/chroot/buildboxworker-1/opt/remoteexecution/buildboxworker.conf"
};
// clang-format on

void tester(const Worker &worker)
{
    EXPECT_STREQ("dev", worker.d_instanceName.c_str());
    EXPECT_STREQ("http://127.0.0.1:50011", worker.d_casServer.d_url);
    EXPECT_STREQ("dev", worker.d_casServer.d_instanceName);
    EXPECT_STREQ("http://100.70.37.178:50051", worker.d_botsServer.d_url);
    EXPECT_STREQ("dev", worker.d_botsServer.d_instanceName);
    EXPECT_STREQ("debug", worker.d_logLevel.c_str());
    EXPECT_STREQ("4", worker.d_botsServer.d_retryLimit);
    EXPECT_STREQ("1000", worker.d_botsServer.d_retryDelay);
    EXPECT_STREQ("4", worker.d_casServer.d_retryLimit);
    EXPECT_STREQ("1000", worker.d_casServer.d_retryDelay);
    EXPECT_EQ(3, worker.d_requestTimeout);
    EXPECT_EQ("/opt/remoteexecution/buildbox-run-hosttools",
              worker.d_runnerCommand);
    EXPECT_THAT(worker.d_extraRunArgs,
                ElementsAre("--prefix-staged-dir", "--use-localcas",
                            "--userchroot-bin=/bb/dbldroot/bin/userchroot"));
    EXPECT_THAT(worker.d_platform,
                ElementsAre(Pair("OSFamily", "linux"), Pair("ISA", "x86-64"),
                            Pair("chrootRootDigest",
                                 "8533ec9ba7494cc8295ccd0bfdca08457421a28b"
                                 "4e92c8eb18e7178fb400f5d4/930"),
                            Pair("chrootRootDigest",
                                 "1e7088e7aca9e8713a84122218a89c8908b39b57"
                                 "97d32170f1afa6e474b9ade6/930")));
    EXPECT_STREQ("/dbldroot/data/chroot/buildboxworker-1/opt/remoteexecution/"
                 "buildboxworker.conf",
                 worker.d_configFileName.c_str());
}

TEST(ParserTest, BasicTest)
{
    CmdLineSpec spec;
    buildboxcommon::CommandLine commandLine(spec.d_spec);

    EXPECT_TRUE(
        commandLine.parse(sizeof(argvTest) / sizeof(const char *), argvTest));

    buildboxcommon::buildboxcommonmetrics::MetricsConfigType metricsConfig;
    Worker worker(commandLine, spec.d_botId, &metricsConfig);

    tester(worker);
}
